import Node


class Graph:
    vertices = {}

    def add_node(self, node):
        try:
            if isinstance(node, Node) and node.name not in self.vertices:
                self.vertices[node.name] = node
                return True
        except TypeError:
            return TypeError

    def add_edges(self, nodeu, nodev):
        if nodeu in self.vertices and nodev in self.vertices:
            for key, value in self.vertices.items():
                if key == nodeu:
                    value.add_neighbor(nodev)
                if key == nodev:
                    value.add_neighbor(nodeu)
            return True
        else:
            return False

    def print_graph(self):
        for key in sorted(list(self.vertices.keys())):
            print(key + str(self.vertices[key].neighbors))

    def bfs(self, vert):
        return None