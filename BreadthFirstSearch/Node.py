class Node:
    def __init__(self, name):
        self.name = name
        self.neighbors = list()
        self.distance = 1337
        self.visited = False

    def add_neighbor(self, neighbor):
        if neighbor not in self.neighbors:
            self.neighbors.append(neighbor)
            self.neighbors.sort()  # sort it not sure why so revisit.