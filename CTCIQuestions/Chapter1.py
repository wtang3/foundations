# Question 1 write an algorithm to ensure that a string has all unique characters

# Solution 1 sort the string as we sort it ensure that we do not have duplicates.
#            if sort is successful we have a unique string.

# I wonder if using set is valid in python

# Question 1.1
def is_unique(str):
    return len(str) == len(set(str))
