import unittest
from Chapter1 import is_unique

class Tests(unittest.TestCase):

    def test_question_1(self):
        self.assertTrue(is_unique("Testing"))
        self.assertFalse(is_unique("Massachusetts"))

