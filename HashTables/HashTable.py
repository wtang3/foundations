# In python hash tables are implemented using dictionaries.
# Hash tables are synchronized while a hash map is not.

dict = {'a' : '1'}

dict[2] = 2
dict['c'] = "dog"

for i, j in dict.iteritems():
    print i, j

# will only allow you to hash a literal or string
print hash(dict['a'])