## m x n matrix print the 1's for the outsides and 
## 0s for the insides
##
##  3 x 3 
##  111
##  101
##  111

import numpy as np

def magic(m,n):

  if m != n:
    return "Not a magic square"

  return [[1 if j==0 or j==m-1 or i==0 or i == n-1 else 0 
            for i in range(0,n)] 
            for j in range(0,m)]

def __init__():
  print(np.matrix(magic(6,6)))


__init__()
