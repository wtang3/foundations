def __init__():
  a = [1,2,3,4,5,6,7,8]
  b = [7,8,1,2,3,4,5,6]

  c = [2,3,4,5,6,7,8,9]

  print(isRotation(a,b))

  print(isRotation(a,c))

  
def isRotation(a,b):
  if len(a) != len(b):
    return False
  
  ## initialize the search
  indexA, indexB = a[0], -1

  ## find where indexB matches indexA
  for index, value in enumerate(b):
    if b[index] == indexA:
      indexB = index
      break

  ## enumerate through and find the matching location.
  for index, value in enumerate(a):
    j = (index + indexB) % len(a)

    if a[index] != b[j]:
      return False

    return True

__init__()