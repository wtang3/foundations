package Algorithms;

/**
 * Created by 0x1CEB00DA on 3/29/2016.
 */
public class Chapter1 {
    public static boolean isUniqueChars2(String str) {
        boolean[] char_set = new boolean[256];
        for (int i = 0; i < str.length(); i++) {
            int val = str.charAt(i);
            if (char_set[val]) return false;
            char_set[val] = true;
        }
        return true;
    }


    public static boolean isUniqueChars(String str) {
        int checker = 0;
        for (int i = 0; i < str.length(); ++i) {
            int val = str.charAt(i) - 'a';
            if ((checker & (1 << val)) > 0) {
                return false;
            }

            checker |= (1 << val);
        }
        return true;
    }

    public static void main(String[] args){

        String test = "dogg";
        String test2 = "abcedagoz";

        boolean flag = isUniqueChars(test);
        System.out.println(flag);
    /*
        boolean flag = isUniqueChars2(test);
        boolean flag2 = isUniqueChars2(test2);
        System.out.println(flag);
        System.out.println(flag2);*/
    }
}
