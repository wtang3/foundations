package LinkedListImplementation;

/**
 * Created by William Tang on 4/2/2016.
 */

public class LinkedListImplementation<E> {
    private Node<E> head = null;
    private Node<E> tail = null;
    private int size = 0;

    public LinkedListImplementation(){
        this.head = new Node<E>(null);
    }

    public void add(E data){
        Node<E> temp = new Node<E>(data);
        Node current = head;

        while(current.getNext() != null){
            current = current.getNext();
        }
        current.setNext(temp);
        tail = temp;
        size ++;
    }

    public boolean remove(int index){
        if(index <=0 || index > size()){
            return false;
        }

        Node current = head;
        // The reason for a linkedlist being O(n)
        for(int i = 1; i < index; i++){
            if(current.getNext() == null){
                return false;
            }

            current = current.getNext();
        }
        current.setNext(current.getNext().getNext());
        size--;
        return true;
    }

    public E get(int index){
        if(index <= 0){
            return null;
        }
        Node current = head.getNext();
        // The reason for a linkedlist being O(n)
        for(int i = 1; i < index; i++){
            if(current.getNext() == null){
                return null;
            }
            current = current.getNext();
        }

        return (E) current.getData();
    }

    // GC should take care of the other nodes
    public void clear(){
        head = null;
        System.gc();
    }

    public String toString(){
        String output = "";
        if(head != null) {
            Node current = head.getNext();

            while (current != null) {
                output += current.getData().toString() + "\n";
                current = current.getNext();
            }
        }
        return output;
    }

    public int size(){
        return size;
    }
}
