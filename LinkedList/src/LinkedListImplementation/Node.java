package LinkedListImplementation;

/**
 * Created by 0x1CEB00DA on 4/2/2016.
 */
public class Node<T> {
    private T data;
    private Node<T> next;

    public Node(T _data){
        this.data = _data;
        this.next = null;
    }

    public Node(T _data, Node<T> _next){
        this.data = _data;
        this.next = _next;
    }

    public T getData(){
        return data;
    }

    public void setData(T _data){
        data = _data;
    }

    public Node<T> getNext(){
        return next;
    }

    public void setNext(Node<T> _next){
        this.next = _next;
    }
}
