package Tests;

import java.util.*;

import Algorithms.MergeSort;
import org.junit.*;

public class Tests {

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void mergeSortRecursively() {

        MergeSort mergeSort = new MergeSort();

        Random r = new Random();

        List<Integer> temp = new ArrayList<Integer>();

        int length = 10;
        int array[] = new int[length];

        for (int i = 0; i < length; i++) {
            array[i] = (r.nextInt(100 - 0 + 1) + 0);
        }

        for (int i : array) {
            temp.add(i);
        }

        array = mergeSort.sortRecursively(array);

        Collections.sort(temp);

        int expected[] = new int[temp.size()];

        for (int i = 0; i < temp.size(); i++) {
            expected[i] = temp.get(i);
        }

        Assert.assertArrayEquals(expected, array);
    }

    @Test
    public void testMergeSortRecurseTime(){
        MergeSort mergeSort = new MergeSort();

        Random r = new Random();

        int length = 10000;
        int array[] = new int[length];

        for (int i = 0; i < length; i++) {
            array[i] = (r.nextInt(100 - 0 + 1) + 0);
        }

        array = mergeSort.sortRecursively(array);
    }

    @Test
    public void javaSortTime(){
        List<Integer> temp = new ArrayList<Integer>();
        Random r = new Random();
        int length = 10000;
        for (int i = 0; i < length; i++) {
            temp.add((r.nextInt(100 - 0 + 1) + 0));
        }
        Collections.sort(temp);
    }

    @Test
    public void mergeSort(){
		//MergeSort mergeSort = new MergeSort();
		Random r = new Random();
		int length = 10;
		int array [] = new int[length];

		for(int i = 0; i< length; i++){
			array[i]=(r.nextInt(100 - 0 + 1) + 0);
		}

        int temp [] = new int[length];
        temp = array;


		for(int i:array){
			System.out.format("%d ",i);
		}
	}

}
