def mergesort_recurse(a):
    if (len(a) > 1):
        mid = len(a) // 2
        left = a[:mid]   #shows the left side of the list
        right = a[mid:]  #shows the right side of the list

        # un comment below to see the visual representation
        # of it splitting
        #print left
        #print right

        mergesort_recurse(left)
        mergesort_recurse(right)

        i = 0
        j = 0
        k = 0

        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                a[k] = left[i]
                i += 1
            else:
                a[k] = right[j]
                j += 1

            k += 1

        while i < len(left):
            a[k] = left[i]
            i += 1
            k += 1

        while j < len(right):
            a[k] = right[j]
            j += 1
            k += 1

        return a

def bottomup_mergesort():
    return None
