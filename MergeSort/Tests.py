import unittest
import random

from MergeSort import mergesort_recurse, bottomup_mergesort

class Tests(unittest.TestCase):
    def test_mergesort_recursively(self):
        length = 11
        a = list()
        for x in range(0, length):
            a.insert(x, random.randrange(0, 100))
        b = mergesort_recurse(a)
        expected = all(b[i] <= b[i + 1] for i in xrange(len(b) - 1))
        self.assertTrue(expected)

    def test_mergesort_recursively_negative(self):
        a = [-1, -300, 30, 40, -100, 6, 2, 5, -3]
        b = mergesort_recurse(a)
        expected = all(b[i] <= b[i + 1] for i in xrange(len(b) - 1))
        self.assertTrue(expected)

    def test_bottom_up_mergesort(self):
        l = [1, 2, 36, 4, 5]

