from distutils.core import setup

setup(
    name='Foundations',
    version='0.1',
    packages=['MergeSort',
              'BreadthFirstSearch',
              'HashTables',
              'CTCIQuestions'],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read()
)
